#include <utility>

using Point = std::pair<int, int>;

const char SNAKE_CHAR = '*';
const char FOOD_CHAR = '@';
const char AREA_CHAR = ' ';

const int MAX_WIDTH = 1000;
const int MAX_HEIGHT = 1000;
const int SLEEP_TIME_MS = 250;